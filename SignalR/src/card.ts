﻿class Card {
    text: string;
    cardTypeText: string;
    cardId: string;

    constructor(text: string, type:string, cardId:string) {
        this.text = text;
        this.cardTypeText = type;
        this.cardId = cardId;
    }

}
 