"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("./css/main.css");
var signalR = require("@microsoft/signalr");
var divMessages = document.querySelector("#divMessages");
var tbMessage = document.querySelector("#tbMessage");
var tbUser = document.querySelector("#tbUser");
var btnSend = document.querySelector("#btnSend");
var btnSaveUser = document.querySelector("#btnSaveUser");
var btnPick = document.querySelector("#btnPick");
var btnNewParty = document.querySelector("#btnNewParty");
var btnPerk1 = document.querySelector("#btnPerk1");
var btnPerk2 = document.querySelector("#btnPerk2");
var btnPerk3 = document.querySelector("#btnPerk3");
var btnValidatePerks = document.querySelector("#btnValidatePerks");
var btnProblem1 = document.querySelector("#btnProblem1");
var btnProblem2 = document.querySelector("#btnProblem2");
var btnProblem3 = document.querySelector("#btnProblem3");
var btnNewTour = document.querySelector("#btnNewTour");
var btnValidateProblem = document.querySelector("#btnValidateProblem");
var selectableUser = document.querySelector("#selectableUser");
var user = null;
var selectedProblem = null;
var selectedPerksCount = 0;
var selectedPerk1 = null;
var selectedPerk2 = null;
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/hub")
    .build();
connection.on("messageReceived", function (username, message) {
    log("<div><b>" + username + " :</b> " + message + "</div>");
});
connection.on("messageUserAddedReceived", function (username, numberOfUsers, users) {
    log(username + " a \u00E9t\u00E9 ajout\u00E9. " + numberOfUsers + " utilisateurs en jeu.");
    selectableUser.innerHTML = '';
    users.forEach(function (value) {
        var option = document.createElement("option");
        option.setAttribute("value", value);
        option.innerHTML = value;
        if (user == value) {
            option.setAttribute("disabled", "");
        }
        selectableUser.appendChild(option);
    });
});
connection.on("messagePickCardsReceived", function (username, perkCards, problemCards, remainingCards) {
    log(username + " pioche, il reste " + remainingCards + " cartes.");
    if (user == username) {
        btnPerk1.innerHTML = perkCards[0].text;
        btnPerk2.innerHTML = perkCards[1].text;
        btnPerk3.innerHTML = perkCards[2].text;
        btnProblem1.innerHTML = problemCards[0].text;
        btnProblem2.innerHTML = problemCards[1].text;
        btnProblem3.innerHTML = problemCards[2].text;
    }
});
connection.on("messageCreatePartyReceived", function () {
    divMessages.innerHTML = '';
    log("Une nouvelle partie commence");
    btnPick.removeAttribute("disabled");
});
connection.on("messageNewTourReceived", function () {
    divMessages.innerHTML = '';
    log("Un nouveau tour commence");
    btnPick.removeAttribute("disabled");
});
connection.on("messageValidateProblemReceived", function (user, selectedUser, selectedProblem) {
    log("<div><b>" + user + "</b> pose <b>" + selectedProblem + "</b> devant <b>" + selectedUser + "</b></div>");
});
connection.on("messageValidatePerksReceived", function (username, selectedPerk1, selectedPerk2) {
    log("<div><b>" + username + "</b> a les avantages: <ul>\n  <li>" + selectedPerk1 + "</li>\n  <li>" + selectedPerk2 + "</li>\n</ul> </div>");
});
connection.start().catch(function (err) { return document.write(err); });
btnSaveUser.addEventListener("click", saveUser);
btnSend.addEventListener("click", send);
btnNewParty.addEventListener("click", function (e) {
    connection.send("createNewParty");
});
btnNewTour.addEventListener("click", function (e) {
    connection.send("launchNewTour");
});
btnPick.addEventListener("click", function (e) {
    if (checkUser()) {
        connection
            .send("pickCardsMessage", user)
            .then(function () {
            btnPick.setAttribute("disabled", "");
        });
    }
});
btnValidateProblem.addEventListener("click", function (e) {
    if (checkUser()) {
        if (selectedProblem == null) {
            log("Il faut choisir une carte probl\u00E8me");
        }
        else {
            var sel = selectableUser.selectedIndex;
            var opt = selectableUser.options[sel];
            var selectedUser = opt.text;
            connection
                .send("validateProblem", user, selectedUser, selectedProblem)
                .then(function () {
                btnValidateProblem.setAttribute("disabled", "");
                btnProblem1.setAttribute("disabled", "");
                btnProblem2.setAttribute("disabled", "");
                btnProblem3.setAttribute("disabled", "");
            });
        }
    }
});
btnPerk1.addEventListener("click", function (e) {
    btnPerk1.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk2.setAttribute("class", "perk");
        btnPerk3.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk1.innerHTML);
});
btnPerk2.addEventListener("click", function (e) {
    btnPerk2.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk1.setAttribute("class", "perk");
        btnPerk3.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk2.innerHTML);
});
btnPerk3.addEventListener("click", function (e) {
    btnPerk3.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk1.setAttribute("class", "perk");
        btnPerk2.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk3.innerHTML);
});
btnValidatePerks.addEventListener("click", function (e) {
    connection
        .send("ValidatePerks", user, selectedPerk1, selectedPerk2)
        .then(function () {
        btnPerk1.setAttribute("disabled", "");
        btnPerk2.setAttribute("disabled", "");
        btnPerk3.setAttribute("disabled", "");
        btnValidatePerks.setAttribute("disabled", "");
    });
});
btnProblem1.addEventListener("click", function (e) {
    btnProblem1.setAttribute("class", "selected-problem");
    btnProblem2.setAttribute("class", "problem");
    btnProblem3.setAttribute("class", "problem");
    selectedProblem = btnProblem1.innerHTML;
});
btnProblem2.addEventListener("click", function (e) {
    btnProblem1.setAttribute("class", "problem");
    btnProblem2.setAttribute("class", "selected-problem");
    btnProblem3.setAttribute("class", "problem");
    selectedProblem = btnProblem2.innerHTML;
});
btnProblem3.addEventListener("click", function (e) {
    btnProblem1.setAttribute("class", "problem");
    btnProblem2.setAttribute("class", "problem");
    btnProblem3.setAttribute("class", "selected-problem");
    selectedProblem = btnProblem3.innerHTML;
});
function eventClick(eventName) {
    if (checkUser()) {
        connection.send("newMessage", user, eventName);
    }
}
function saveUser() {
    user = tbUser.value;
    connection
        .send("addUser", user)
        .then(function () {
        tbUser.setAttribute("readOnly", "true");
        btnSaveUser.setAttribute("disabled", "");
    });
}
function send() {
    connection.send("newMessage", user, tbMessage.value)
        .then(function () { return tbMessage.value = ""; });
}
function checkUser() {
    if (user == null) {
        log('Il te faut un nom de joueur !');
        return false;
    }
    return true;
}
function log(message) {
    var m = document.createElement("div");
    m.innerHTML = message;
    divMessages.appendChild(m);
    divMessages.scrollTop = divMessages.scrollHeight;
}
function setSelectedPerks(value) {
    if (selectedPerk1 == null) {
        selectedPerk1 = value;
    }
    else {
        selectedPerk2 = value;
    }
}
