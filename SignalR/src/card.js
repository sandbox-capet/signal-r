var Card = /** @class */ (function () {
    function Card(text, type, cardId) {
        this.text = text;
        this.cardTypeText = type;
        this.cardId = cardId;
    }
    return Card;
}());
