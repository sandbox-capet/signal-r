﻿import "./css/main.css";
import * as signalR from "@microsoft/signalr";

const divMessages: HTMLDivElement = document.querySelector("#divMessages");
const divUserCards: HTMLDivElement = document.querySelector("#user-cards-zone");

const tbMessage: HTMLInputElement = document.querySelector("#tbMessage");
const tbUser: HTMLInputElement = document.querySelector("#tbUser");

const btnSend: HTMLButtonElement = document.querySelector("#btnSend");
const btnSaveUser: HTMLButtonElement = document.querySelector("#btnSaveUser");
const btnPick: HTMLButtonElement = document.querySelector("#btnPick");
const btnNewParty: HTMLButtonElement = document.querySelector("#btnNewParty");
const btnPerk1: HTMLButtonElement = document.querySelector("#btnPerk1");
const btnPerk2: HTMLButtonElement = document.querySelector("#btnPerk2");
const btnPerk3: HTMLButtonElement = document.querySelector("#btnPerk3");
const btnValidatePerks: HTMLButtonElement = document.querySelector("#btnValidatePerks");
const btnProblem1: HTMLButtonElement = document.querySelector("#btnProblem1");
const btnProblem2: HTMLButtonElement = document.querySelector("#btnProblem2");
const btnProblem3: HTMLButtonElement = document.querySelector("#btnProblem3");
const btnNewTour: HTMLButtonElement = document.querySelector("#btnNewTour");
const btnValidateProblem: HTMLButtonElement = document.querySelector("#btnValidateProblem");

const selectableUser: HTMLSelectElement = document.querySelector("#selectableUser");

let user = null;
let selectedProblem = null;
let selectedPerksCount = 0;
let selectedPerk1 = null;
let selectedPerk2 = null;

const connection = new signalR.HubConnectionBuilder()
    .withUrl("/hub")
    .build();

connection.on("messageReceived", (username: string, message: string) => {
    log(`<div><b>${username} :</b> ${message}</div>`);
});

connection.on("messageUserAddedReceived", (username: string, numberOfUsers: number, users: string[]) => {
    log(`${username} a été ajouté. ${numberOfUsers} utilisateurs en jeu.`);
    selectableUser.innerHTML = '';
    users.forEach(function (value) {
        let option = document.createElement("option");
        option.setAttribute("value", value);
        option.innerHTML = value;
        if (user == value) {
            option.setAttribute("disabled", "");
        }
        selectableUser.appendChild(option);
    });
});

connection.on("messagePickCardsReceived", (username: string, perkCards: Card[], problemCards: Card[], remainingCards: number) => {
    log(`${username} pioche, il reste ${remainingCards} cartes.`);
    if (user == username) {
        divUserCards.setAttribute("class","input-zone");
        btnPerk1.innerHTML = perkCards[0].text;
        btnPerk2.innerHTML = perkCards[1].text;
        btnPerk3.innerHTML = perkCards[2].text;
        btnProblem1.innerHTML = problemCards[0].text;
        btnProblem2.innerHTML = problemCards[1].text;
        btnProblem3.innerHTML = problemCards[2].text;
    }
});

connection.on("messageCreatePartyReceived", () => {
    divMessages.innerHTML = '';
    log(`Une nouvelle partie commence`);
    btnPick.removeAttribute("disabled");
    divUserCards.setAttribute("class","hidden-zone");
});

connection.on("messageNewTourReceived", () => {
    divMessages.innerHTML = '';
    log(`Un nouveau tour commence`);
    btnPick.removeAttribute("disabled");
    divUserCards.setAttribute("class","hidden-zone");
});

connection.on("messageValidateProblemReceived", (user: string, selectedUser: string, selectedProblem: string) => {
    log(`<div><b>${user}</b> pose <b>${selectedProblem}</b> devant <b>${selectedUser}</b></div>`);
});

connection.on("messageValidatePerksReceived", (username: string, selectedPerk1: string, selectedPerk2: string) => {
    log(`<div><b>${username}</b> a les avantages: <ul>
  <li>${selectedPerk1}</li>
  <li>${selectedPerk2}</li>
</ul> </div>`);
});

connection.start().catch(err => document.write(err));

btnSaveUser.addEventListener("click", saveUser);
btnSend.addEventListener("click", send);

btnNewParty.addEventListener("click", (e: MouseEvent) => {
    connection.send("createNewParty");
});

btnNewTour.addEventListener("click", (e: MouseEvent) => {
    connection.send("launchNewTour");
});

btnPick.addEventListener("click", (e: MouseEvent) => {
    if (checkUser()) {
        connection
            .send("pickCardsMessage", user)
            .then(() => {
                btnPick.setAttribute("disabled", "");
            });
    }
});

btnValidateProblem.addEventListener("click", (e: MouseEvent) => {
    if (checkUser()) {

        if (selectedProblem == null) {
            log(`Il faut choisir une carte problème`);
        } else {
            let sel = selectableUser.selectedIndex;
            let opt = selectableUser.options[sel];
            let selectedUser = opt.text;

            connection
                .send("validateProblem", user, selectedUser, selectedProblem)
                .then(() => {
                    btnValidateProblem.setAttribute("disabled", "");
                    btnProblem1.setAttribute("disabled", "");
                    btnProblem2.setAttribute("disabled", "");
                    btnProblem3.setAttribute("disabled", "");
                });
        }
    }
});

btnPerk1.addEventListener("click", (e: MouseEvent) => {
    btnPerk1.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk2.setAttribute("class", "perk");
        btnPerk3.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk1.innerHTML);
});

btnPerk2.addEventListener("click", (e: MouseEvent) => {
    btnPerk2.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk1.setAttribute("class", "perk");
        btnPerk3.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk2.innerHTML);
});

btnPerk3.addEventListener("click", (e: MouseEvent) => {
    btnPerk3.setAttribute("class", "selected-perk");
    selectedPerksCount++;
    if (selectedPerksCount == 3) {
        btnPerk1.setAttribute("class", "perk");
        btnPerk2.setAttribute("class", "perk");
        selectedPerk1 = null;
        selectedPerk2 = null;
        selectedPerksCount -= 2;
    }
    setSelectedPerks(btnPerk3.innerHTML);
});

btnValidatePerks.addEventListener("click", (e: MouseEvent) => {
    connection
        .send("ValidatePerks", user,selectedPerk1,selectedPerk2)
        .then(() => {
            btnPerk1.setAttribute("disabled", "");
            btnPerk2.setAttribute("disabled", "");
            btnPerk3.setAttribute("disabled", "");
            btnValidatePerks.setAttribute("disabled", "");
        });
});

btnProblem1.addEventListener("click", (e: MouseEvent) => {
    btnProblem1.setAttribute("class", "selected-problem");
    btnProblem2.setAttribute("class", "problem");
    btnProblem3.setAttribute("class", "problem");
    selectedProblem = btnProblem1.innerHTML;
});

btnProblem2.addEventListener("click", (e: MouseEvent) => {
    btnProblem1.setAttribute("class", "problem");
    btnProblem2.setAttribute("class", "selected-problem");
    btnProblem3.setAttribute("class", "problem");
    selectedProblem = btnProblem2.innerHTML;
});

btnProblem3.addEventListener("click", (e: MouseEvent) => {
    btnProblem1.setAttribute("class", "problem");
    btnProblem2.setAttribute("class", "problem");
    btnProblem3.setAttribute("class", "selected-problem");
    selectedProblem = btnProblem3.innerHTML;
});


function eventClick(eventName: string) {
    if (checkUser()) {
        connection.send("newMessage", user, eventName);
    }
}

function saveUser() {
    user = tbUser.value;
    connection
        .send("addUser", user)
        .then(() => {
            tbUser.setAttribute("readOnly", "true");
            btnSaveUser.setAttribute("disabled", "");
        });

}

function send() {
    connection.send("newMessage", user, tbMessage.value)
        .then(() => tbMessage.value = "");
}

function checkUser(): boolean {
    if (user == null) {
        log('Il te faut un nom de joueur !');
        return false;
    }
    return true;
}

function log(message: string) {
    let m = document.createElement("div");
    m.innerHTML = message;
    divMessages.appendChild(m);
    divMessages.scrollTop = divMessages.scrollHeight;
}


function setSelectedPerks(value: string) {
    if (selectedPerk1 == null) {
        selectedPerk1 = value;
    } else {
        selectedPerk2 = value;
    }
}

