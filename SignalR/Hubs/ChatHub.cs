﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Serilog;
using SignalR.Models;
using SignalR.Repositories;

namespace SignalR.Hubs
{
    public class ChatHub : Hub
    {
        private readonly CardRepository cardRepository;
        private readonly UserRepository userRepository;

        public ChatHub()
        {
            cardRepository = new CardRepository();
            userRepository = new UserRepository();
            cardRepository.LoadCards();
        }

        public async Task AddUser(string userName)
        {
            Log.Information("AddUser called");
            var numberOfUsers = userRepository.Add(userName);
            var allUsers = userRepository.GetAll();
            await Clients.All.SendAsync("messageUserAddedReceived", userName, numberOfUsers, allUsers);
        }

        public async Task NewMessage(string username, string message)
        {
            Log.Information("NewMessage called");
            await Clients.All.SendAsync("messageReceived", username, message);
        }

        public async Task PickCardMessage(string username)
        {
            Log.Information("PickCardMessage called");
            var perkCard = cardRepository.PickCard(CardType.Perk);
            await Clients.All.SendAsync("messagePickCardReceived", username, perkCard);
        }

        public async Task PickCardsMessage(string username)
        {
            Log.Information("PickCardsMessage called");
            var perkCards = cardRepository.PickCards(CardType.Perk, 3);
            var problemsCards = cardRepository.PickCards(CardType.Problem, 3);
            var remainingCards = cardRepository.CountRemainingCards();
            await Clients.All.SendAsync("messagePickCardsReceived", username, perkCards, problemsCards, remainingCards);
        }

        public async Task CreateNewParty()
        {
            Log.Information("CreateNewParty called");
            userRepository.Clean();
            cardRepository.LoadCards();
            await Clients.All.SendAsync("messageCreatePartyReceived");
        }

        public async Task LaunchNewTour()
        {
            Log.Information("LaunchNewTour called");
            await Clients.All.SendAsync("messageNewTourReceived");
        }

        public async Task ValidateProblem(string user, string selectedUser, string selectedProblem)
        {
            Log.Information("ValidateProblem called");
            await Clients.All.SendAsync("messageValidateProblemReceived", user, selectedUser, selectedProblem);
        }
        
        public async Task ValidatePerks(string user, string selectedPerk1, string selectedPerk2)
        {
            Log.Information("ValidatePerks called");
            await Clients.All.SendAsync("messageValidatePerksReceived", user, selectedPerk1, selectedPerk2);
        }
    }
}