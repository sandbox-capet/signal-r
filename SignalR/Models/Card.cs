﻿using System;
using System.ComponentModel;

namespace SignalR.Models
{
    public class Card
    {
        public Guid CardId { get; set; }
        public string Text { get; set; }
        public CardType CardType { get; set; }

        public string CardTypeText => CardType == CardType.Perk ? "Avantage" : "Problème";

        public Card(string text, CardType cardType)
        {
            CardId = Guid.NewGuid();
            Text = text;
            CardType = cardType;
        }
    }

    public enum CardType
    {
        Perk = 1,
        Problem = 2
    }
}