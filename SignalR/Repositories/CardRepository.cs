﻿using System.Collections.Generic;
using System.Linq;
using SignalR.Extensions;
using SignalR.Models;

namespace SignalR.Repositories
{
    public class CardRepository
    {
        public static List<Card> Cards { get; set; }

        public void LoadCards()
        {
            Cards = new List<Card>
            {
                new Card("Tu gagnes 1€ toutes les secondes", CardType.Perk),
                new Card("Tu gagnes 20€ toutes les secondes", CardType.Perk),
                new Card("Tu gagnes 300€ toutes les secondes", CardType.Perk),
                new Card("Tu gagnes 4000€ toutes les secondes", CardType.Perk),
                new Card("Tu gagnes 50000€ toutes les secondes", CardType.Perk),
                new Card("Tu gagnes 1€ toutes les minutes", CardType.Perk),
                new Card("Tu gagnes 20€ toutes les minutes", CardType.Perk),
                new Card("Tu gagnes 300€ toutes les minutes", CardType.Perk),
                new Card("Tu gagnes 4000€ toutes les minutes", CardType.Perk),
                new Card("Tu gagnes 50000€ toutes les minutes", CardType.Perk),

                new Card("Ton pire enemi gagne 1€ toutes les secondes", CardType.Problem),
                new Card("Ton pire enemi gagne 20€ toutes les secondes", CardType.Problem),
                new Card("Ton pire enemi gagne 300€ toutes les secondes", CardType.Problem),
                new Card("Ton pire enemi gagne 4000€ toutes les secondes", CardType.Problem),
                new Card("Ton pire enemi gagne 50000€ toutes les secondes", CardType.Problem),
                new Card("Ton pire enemi gagne 1€ toutes les minutes", CardType.Problem),
                new Card("Ton pire enemi gagne 20€ toutes les minutes", CardType.Problem),
                new Card("Ton pire enemi gagne 300€ toutes les minutes", CardType.Problem),
                new Card("Ton pire enemi gagne 4000€ toutes les minutes", CardType.Problem),
                new Card("Ton pire enemi gagne 50000€ toutes les minutes", CardType.Problem),
            };
        }

        public void Add(Card card)
        {
            Cards.Add(card);
        }

        public Card PickCard(CardType cardType)
        {
            var card = PickCards(cardType, 1).Single();
            Cards.RemoveAll(p => p.CardId == card.CardId);
            return card;
        }

        public IEnumerable<Card> PickCards(CardType cardType, int count)
        {
            var cardsTyped = Cards.Where(p => p.CardType == cardType);
            var cards = cardsTyped.PickRandom(count).ToList();
            foreach (var card in cards)
            {
                Cards.RemoveAll(p => p.CardId == card.CardId);
            }
            return cards;
        }

        public int CountRemainingCards()
        {
            return Cards.Count;
        }
    }
}