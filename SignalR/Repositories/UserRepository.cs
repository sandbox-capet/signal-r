﻿using System.Collections.Generic;

namespace SignalR.Repositories
{
    public class UserRepository
    {
        public static List<string> Users { get; set; } = new List<string>();

        public int Add(string userName)
        {
            Users.Add(userName);
            return Users.Count;
        }
        
        public void Clean()
        {
            Users.Clear();
        }
        
        public List<string> GetAll()
        {
            return Users;
        }
    }
}